# This overlay, when applied to nixpkgs, adds the final neovim derivation to nixpkgs.
{inputs}: final: prev:
with final.pkgs.lib; let
  pkgs = final;

  # Use this to create a plugin from a flake input
  mkNvimPlugin = src: pname:
    pkgs.vimUtils.buildVimPlugin {
      inherit pname src;
      version = src.lastModifiedDate;
    };

  # Make sure we use the pinned nixpkgs instance for wrapNeovimUnstable,
  # otherwise it could have an incompatible signature when applying this overlay.
  pkgs-wrapNeovim = inputs.nixpkgs.legacyPackages.${pkgs.system};

  # This is the helper function that builds the Neovim derivation.
  mkNeovim = pkgs.callPackage ./mkNeovim.nix { inherit pkgs-wrapNeovim; };

  # A plugin can either be a package or an attrset, such as
  # { plugin = <plugin>; # the package, e.g. pkgs.vimPlugins.nvim-cmp
  #   config = <config>; # String; a config that will be loaded with the plugin
  #   # Boolean; Whether to automatically load the plugin as a 'start' plugin,
  #   # or as an 'opt' plugin, that can be loaded with `:packadd!`
  #   optional = <true|false>; # Default: false
  #   ...
  # }

  all-plugins = with pkgs.vimPlugins; [
    # Plugins from nixpkgs go in here.
    # https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=vimPlugins
    luasnip # snippets | https://github.com/l3mon4d3/luasnip/
    nvim-treesitter.withAllGrammars

    # Nvim-cmp (autocompletion) and extensions
    cmp-buffer # current buffer as completion source | https://github.com/hrsh7th/cmp-buffer/
    cmp-cmdline # cmp command line suggestions
    cmp-cmdline-history # cmp command line history suggestions
    cmp_luasnip # snippets autocompletion extension for nvim-cmp | https://github.com/saadparwaiz1/cmp_luasnip/
    cmp-nvim-lsp # LSP as completion source | https://github.com/hrsh7th/cmp-nvim-lsp/
    cmp-nvim-lsp-signature-help # https://github.com/hrsh7th/cmp-nvim-lsp-signature-help/
    cmp-nvim-lua # neovim lua API as completion source | https://github.com/hrsh7th/cmp-nvim-lua/
    cmp-path # file paths as completion source | https://github.com/hrsh7th/cmp-path/
    lspkind-nvim # vscode-like LSP pictograms | https://github.com/onsails/lspkind.nvim/
    nvim-cmp # https://github.com/hrsh7th/nvim-cmp

    # Git integration plugins
    diffview-nvim # https://github.com/sindrets/diffview.nvim/
    gitsigns-nvim # https://github.com/lewis6991/gitsigns.nvim/
    neogit # https://github.com/TimUntersberger/neogit/
    vim-fugitive # https://github.com/tpope/vim-fugitive/

    # Telescope and extensions
    telescope-fzf-native-nvim # https://github.com/nvim-telescope/telescope-fzf-native.nvim
    telescope-nvim # https://github.com/nvim-telescope/telescope.nvim/

    # UI
    alpha-nvim ## https://github.com/goolord/alpha-nvim/
    bufferline-nvim # https://github.com/akinsho/bufferline.nvim/
    indent-blankline-nvim # https://github.com/lukas-reineke/indent-blankline.nvim/
    lualine-nvim # Status line | https://github.com/nvim-lualine/lualine.nvim/
    noice-nvim # https://github.com/folke/noice.nvim
    nvim-treesitter-context # nvim-treesitter-context
    statuscol-nvim # Status column | https://github.com/luukvbaal/statuscol.nvim/

    # Navigation/editing enhancement plugins
    eyeliner-nvim # Highlights unique characters for f/F and t/T motions | https://github.com/jinh0/eyeliner.nvim
    nvim-autopairs # https://github.com/windwp/nvim-autopairs/
    nvim-surround # https://github.com/kylechui/nvim-surround/
    nvim-treesitter-textobjects # https://github.com/nvim-treesitter/nvim-treesitter-textobjects/
    nvim-ts-context-commentstring # https://github.com/joosepalviste/nvim-ts-context-commentstring/
    vim-unimpaired # predefined ] and [ navigation keymaps | https://github.com/tpope/vim-unimpaired/

    # Useful utilities
    nvim-unception # Prevent nested neovim sessions | nvim-unception
    oil-nvim # https://github.com/stevearc/oil.nvim/
    which-key-nvim # https://github.com/folke/which-key.nvim
    
    # Colorschemes
    base16-nvim # https://github.com/RRethy/base16-nvim/

    # Libraries that other plugins depend on
    nui-nvim
    nvim-notify
    nvim-web-devicons
    plenary-nvim
    sqlite-lua
    vim-repeat

    # Bleeding-edge plugins from flake inputs
    # (mkNvimPlugin inputs.wf-nvim "wf.nvim") # (example) keymap hints | https://github.com/Cassin01/wf.nvim
  ];

  extraPackages = with pkgs; [
    # language servers, etc.
    lua-language-server
    nil # nix LSP
  ];
in {
  # This is the neovim derivation
  # returned by the overlay
  nvim-pkg = mkNeovim {
    plugins = all-plugins;
    inherit extraPackages;
  };

  # This can be symlinked in the devShell's shellHook
  nvim-luarc-json = final.mk-luarc-json {
    plugins = all-plugins;
  };

  # You can add as many derivations as you like.
  # Use `ignoreConfigRegexes` to filter out config
  # files you would not like to include.
  #
  # For example:
  #
  # nvim-pkg-no-telescope = mkNeovim {
  #   plugins = [];
  #   ignoreConfigRegexes = [
  #     "^plugin/telescope.lua"
  #     "^ftplugin/.*.lua"
  #   ];
  #   inherit extraPackages;
  # };
}
