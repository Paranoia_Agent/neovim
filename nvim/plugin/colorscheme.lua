if vim.g.did_load_colorscheme_plugin then
  return
end
vim.g.did_load_colorscheme_plugin = true

require('base16-colorscheme').with_config({
  telescope = false,
  indentblankline = true,
  notify = true,
  ts_rainbow = true,
  cmp = true,
  illuminate = true,
  dapui = true,
})

vim.cmd('colorscheme base16-da-one-gray')
vim.api.nvim_set_hl(0, 'CursorLineNR', { fg = require('base16-colorscheme').colors.base05 })
vim.api.nvim_set_hl(0, 'LineNR', { fg = require('base16-colorscheme').colors.base03 })
