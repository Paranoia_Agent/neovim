if vim.g.did_load_lualine_plugin then
  return
end
vim.g.did_load_lualine_plugin = true

---Indicators for special modes,
---@return string status
local function extra_mode_status()
  -- recording macros
  local reg_recording = vim.fn.reg_recording()
  if reg_recording ~= '' then
    return ' @' .. reg_recording
  end
  -- executing macros
  local reg_executing = vim.fn.reg_executing()
  if reg_executing ~= '' then
    return ' @' .. reg_executing
  end
  -- ix mode (<C-x> in insert mode to trigger different builtin completion sources)
  local mode = vim.api.nvim_get_mode().mode
  if mode == 'ix' then
    return '^X: (^]^D^E^F^I^K^L^N^O^Ps^U^V^Y)'
  end
  return ''
end

local default_theme = {
  normal = {
    a = { fg = 'none', bg = 'none' },
    b = { fg = 'none', bg = 'none' },
    c = { fg = 'none', bg = 'none' },
    x = { fg = 'none', bg = 'none' },
    y = { fg = 'none', bg = 'none' },
    z = { fg = 'none', bg = 'none' },
  },
}

require('lualine').setup {
  globalstatus = true,
  options = {
    theme = default_theme,
    component_separators = '',
    section_separators = { left = ''; right = ''},
  },

  sections= {
    lualine_a = { {'mode', fmt = string.lower } },
    lualine_b = { 'filename', 'branch' },
    lualine_c = { 'diff', 'diagnostics', },
    lualine_x = { {extra_mode_status} },
    lualine_y = { 'filetype' },
    lualine_z = { 'location' },
  },

  tabline = {},
  winbar = {},
  extensions = { 'fugitive', 'fzf', 'toggleterm', 'quickfix' },
}
