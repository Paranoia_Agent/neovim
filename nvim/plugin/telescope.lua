if vim.g.did_load_telescope_plugin then
  return
end
vim.g.did_load_telescope_plugin = true

local telescope = require('telescope')
local actions = require('telescope.actions')
local builtin = require('telescope.builtin')

local layout_config = {
  horizontal = {
    width = function(_, max_columns)
      return math.floor(max_columns * 0.99)
    end,
    height = function(_, _, max_lines)
      return math.floor(max_lines * 0.99)
    end,
    prompt_position = 'bottom',
    preview_cutoff = 0,
  },
}

-- Fall back to find_files if not in a git repo
local project_files = function()
  local opts = {} -- define here if you want to define something
  local ok = pcall(builtin.git_files, opts)
  if not ok then
    builtin.find_files(opts)
  end
end

-- Telescope keymaps
-- Find commands
vim.keymap.set('n', '<leader>ff', function()
  builtin.find_files()
end, { desc = '[f]ind [f]iles' })
vim.keymap.set('n', '<leader>fb', builtin.buffers, {desc = '[f]ind [b]uffers'})
vim.keymap.set('n', '<leader>fr', builtin.oldfiles, {desc = '[f]ind [r]ecent files'})
vim.keymap.set('n', '<leader>fp', project_files, {desc = '[f]ind [p]roject files'})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {desc = '[f]ind [l]ive [g]rep'})

-- Search commands
vim.keymap.set('n', '<leader>sr', builtin.registers, { desc = '[s]earch ["]registers'})
vim.keymap.set('n', '<leader>sc', builtin.command_history, { desc = '[s]earch [c]ommand history'})
vim.keymap.set('n', '<leader>sk', builtin.keymaps, {desc = '[s]earch [k]eymaps'})
vim.keymap.set('n', '<leader>sl', builtin.loclist, { desc = '[s]earch [l]oclist'})
vim.keymap.set('n', '<leader>sq', builtin.quickfix, { desc = '[s]earch [q]uickfix list'})
vim.keymap.set('n', '<leader>sgc', builtin.quickfix, { desc = '[s]earch [g]it [c]ommits'})
vim.keymap.set('n', '<leader>sgs', builtin.quickfix, { desc = '[s]earch [g]it [s]tatus'})

telescope.setup {
  defaults = {
    path_display = {
      'truncate',
    },
    layout_strategy = 'horizontal',
    layout_config = layout_config,
    mappings = {
      i = {
        ['<C-q>'] = actions.send_to_qflist,
        ['<C-l>'] = actions.send_to_loclist,
        -- ['<esc>'] = actions.close,
        ['<C-s>'] = actions.cycle_previewers_next,
        ['<C-a>'] = actions.cycle_previewers_prev,
      },
      n = {
        q = actions.close,
      },
    },
    preview = {
      treesitter = true,
    },
    history = {
      path = vim.fn.stdpath('data') .. '/telescope_history.sqlite3',
      limit = 1000,
    },
    color_devicons = true,
    set_env = { ['COLORTERM'] = 'truecolor' },
    prompt_prefix = '   ',
    selection_caret = '  ',
    entry_prefix = '  ',
    initial_mode = 'insert',
    vimgrep_arguments = {
      'rg',
      '-L',
      '--color=never',
      '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case',
    },
  },
  pickers = {
    colorscheme = {
      enable_preview = true;
    },
  },
  extensions = {
    fzf = {
      fuzzy = true,
      override_generic_sorter = true,
      override_file_sorter = true,
      case_mode = "smart_case",
    }
  }
}

require('telescope').load_extension('fzf')
