if vim.g.did_load_alpha_plugin then
  return
end
vim.g.did_load_alpha_plugin = true

local alpha = require("alpha")
local theta = require("alpha.themes.theta")
local dashboard = require("alpha.themes.dashboard")

local header = {
  type = "text",
  val = {
    [[                                                                     ]],
    [[       ████ ██████           █████      ██                     ]],
    [[      ███████████             █████                             ]],
    [[      █████████ ███████████████████ ███   ███████████   ]],
    [[     █████████  ███    █████████████ █████ ██████████████   ]],
    [[    █████████ ██████████ █████████ █████ █████ ████ █████   ]],
    [[  ███████████ ███    ███ █████████ █████ █████ ████ █████  ]],
    [[ ██████  █████████████████████ ████ █████ █████ ████ ██████ ]],
  },
  opts = {
    position = "center",
    hl = "SpecialComment",
    -- wrap = "overflow";
  },
}

local section_mru = {
  type = "group",
  val = {
    {
      type = "text",
      val = "Recent files",
      opts = {
        hl = "Comment",
        shrink_margin = false,
        position = "center",
      },
    },
    { type = "padding", val = 1 },
    {
      type = "group",
      val = function()
        return { theta.mru(0, vim.fn.getcwd(), 10) }
      end,
      opts = { shrink_margin = false },
    },
  },
}

local buttons = {
  type = "group",
  val = {
    { type = "text", val = "Quick links", opts = { hl = "Comment", position = "center" } },
    { type = "padding", val = 1 },
    dashboard.button("n", "󰈚  New file", "<CMD>ene<CR>"),
    dashboard.button("f", "󰺮  Find file", "<CMD>Telescope find_files<CR>"),
    dashboard.button("g", "󰈭  Live grep", "<CMD>Telescope live_grep<CR>"),
    dashboard.button("p", "  Projects", "<CMD>Telescope git_files<CR>"),
    dashboard.button("q", "󰈆  Quit", "<CMD>qa<CR>"),
  },
  position = "center",
}

theta.config = {
	layout = {
		{ type = "padding", val = 8 },
		header,
		{ type = "padding", val = 2 },
		section_mru,
		{ type = "padding", val = 2 },
		buttons,
	},
}

alpha.setup(theta.config)
