if vim.g.did_load_bufferline_plugin then
  return
end
vim.g.did_load_bufferline_plugin = true

local bufferline = require("bufferline")

bufferline.setup{
  options = {
    mode = "buffers",
    style_preset = bufferline.style_preset.minimal,
    themable = true,
    -- none, ordinal, buffer_id or both
    numbers = "none",
    close_command = "bdelete! %d",
    right_mouse_command = "bdelete! %d",
    left_mouse_command = "buffer %d",
    middle_mouse_command = nil,
    indicator = {
      -- icon, underline or none
      style = 'underline',
    },
    buffer_close_icon = '󰅖',
    modified_icon = '●',
    close_icon = '',
    left_trunc_marker = '',
    right_trunc_marker = '',

    max_name_length = 18,
    max_prefix_length = 15,
    truncate_names = true,
    tab_size = 18,
    -- false, nvim_lsp or coc
    diagnostics = "nvim_lsp",
    diagnostics_update_in_insert = true,

    show_buffer_icons = true, -- disable filetype icons for buffers
    show_buffer_close_icons = true,
    show_close_icon = true,
    show_tab_indicators = false,
    show_duplicate_prefix = true, -- whether to show duplicate buffer prefix
    duplicates_across_groups = true, -- whether to consider duplicate paths in different groups as duplicates
    persist_buffer_sort = true, -- whether or not custom sorted buffers should persist
    move_wraps_at_ends = false, -- whether or not the move command "wraps" at the first or last position
    -- slant, slope, thick or thin.
    separator_style = "thick",
    enforce_regular_tabs = false,
    always_show_bufferline = false,
    auto_toggle_bufferline = true,
    hover = {
        enabled = true,
        delay = 0,
        reveal = {'close'}
    },
    -- insert_after_current, insert_at_end, id, extension, relative_directory, directory, tabs or function(buffer_a, buffer_b)
    sort_by = 'insert_at_end',
  }
}
